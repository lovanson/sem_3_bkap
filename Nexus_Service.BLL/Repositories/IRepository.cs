﻿using Nexus_Service.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.BLL
{
    public interface IRepository<T> where T : class,new()
    {
        IEnumerable<T> Get();
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate);
        T Single(Expression<Func<T, bool>> predicate);
        T Get(object id);
        bool Any(Expression<Func<T, bool>> predicate);
        bool Add(T entity);
        bool Edit(T entity);
        bool Remove(object id);
        void Save();

        T SaveObject(T entity);
        string GetMD5(string str);
        string CheckLoginAdmin(string LoginId, string password);
        string CheckLogin(string LoginId, string password);
     
    }
}
