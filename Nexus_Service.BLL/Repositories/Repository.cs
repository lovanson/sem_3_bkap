﻿using Nexus_Service.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.BLL
{
    public class Repository<T> : IRepository<T> where T : class, new()
    {
        private Connect_DB db;
        private DbSet<T> tbl;

        public Repository()
        {
            db = new Connect_DB();
            tbl = db.Set<T>();
        }
        public bool Add(T entity)
        {
            try
            {
                tbl.Add(entity);
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool Any(Expression<Func<T, bool>> predicate)
        {
            return tbl.AsNoTracking().Any(predicate);
        }

        public string CheckLogin(string LoginId, string password)
        {
            throw new NotImplementedException();
        }

        public string CheckLoginAdmin(string LoginId, string password)
        {
            var f_password = GetMD5(password);
            foreach (var item in db.Accounts.ToList())
            {
              
                int lever = 3;
                if (item.LoginId == LoginId && item.password == f_password && item.level != lever )
                {
                    return item.AccountId;
                }
            }
            return null;
        }

        public bool Edit(T entity)
        {
            try
            {
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                return false;

            }
        }

        public IEnumerable<T> Get()
        {
            return tbl.AsEnumerable();
        }

        public T Get(object id)
        {
           return tbl.Find(id);
        }

        public IEnumerable<T> Get(Expression<Func<T, bool>> predicate)
        {
            return tbl.Where(predicate).AsEnumerable();
        }

        public string GetMD5(string str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = md5.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }

        public bool Remove(object id)
        {
            try
            {
                tbl.Remove(Get(id));
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public T SaveObject(T entity)
        {
            try
            {
                tbl.Add(entity);
                Save();
                db.Entry(entity).Reload();
                return entity;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public T Single(Expression<Func<T, bool>> predicate)
        {
            return tbl.SingleOrDefault(predicate);
        }
    }
}
