﻿using Nexus_Service.DAL.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
   public class Connect_DB : DbContext
    {
        public Connect_DB() :base("name = Connect_DB")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Connect_DB, Configuration>("Connect_DB"));
        }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Manufacture> Manufactures { get; set; }
        public virtual DbSet<Device> Devices { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Packet> Packets { get; set; }
        public virtual DbSet<Packet_Detail> Packet_Details { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Branch> Branches { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
      
        public virtual DbSet<Role_Assignment> Role_Assignments { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Feedback> Feedbacks { get; set; }
        public virtual DbSet<Customer_Service> Customer_Services { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Packet_Order> Packet_Orders{ get; set; }
        public virtual DbSet<Payment_Method> Payment_Methods { get; set; }
        public virtual DbSet<Bill> Bills { get; set; }
        public virtual DbSet<Discount> Discounts { get; set; }
        public virtual DbSet<Device_Order> Device_Orders{ get; set; }
        public virtual DbSet<Emp_Order_detail> Emp_Order_Details { get; set; }
        public virtual DbSet<Business> Businesses{ get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<GroupUser> GroupUsers{ get; set; }
        public virtual DbSet<Employee> Employees { get; set; }

        public virtual DbSet<GroupPermission> GroupPermissions{ get; set; }

    }
}
