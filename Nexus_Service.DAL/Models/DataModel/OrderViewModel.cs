﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
   public class OrderViewModel
    {
        public int OrderId { get; set; }
        public int? CusId { get; set; }
       
        public Customer customer { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime Date { get; set; }
        public bool Status { get; set; }
    }
}
