﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
   public class CustomerViewModel
    {
        public int CusId { get; set; }
        public string AccountId { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }
       

        public DateTime DoB { get; set; }

        public string IDcard { get; set; }

        public int CityId { get; set; }
     
        public bool Status { get; set; }
       
    }
}
