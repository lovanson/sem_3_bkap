﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
    public class Payment_Method
    {
        [Key]
        public int PayId { get; set; }
        [Required(ErrorMessage ="Please enter type")]
        public string Type { get; set; }
        public bool Status { get; set; }
        public ICollection<Bill> bills{ get; set; }
    }
}
