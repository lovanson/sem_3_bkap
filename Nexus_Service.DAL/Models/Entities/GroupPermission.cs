﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
   public class GroupPermission
    {
        [Key]
        public int Id { get; set; }
        public int GroupId { get; set; }
        [ForeignKey("GroupId")]
        public GroupUser groupUser { get; set; }
        public int PermissionId { get; set; }
        [ForeignKey("PermissionId")]
        public Permission permission{ get; set; }
    }
}
