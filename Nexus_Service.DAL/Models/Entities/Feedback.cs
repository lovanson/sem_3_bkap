﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
   public  class Feedback
    {
        [Key]
        public int FeedbackId { get; set; }
        public string Content { get; set; }
        public DateTime Date_send { get; set; }
        public bool Status { get; set; }
    }
}
