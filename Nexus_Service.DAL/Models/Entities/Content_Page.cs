﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL.Models.Entities
{
    public class Content_Page
    {
        [Key]
        public int ContentId { get; set; }
        public string Image { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        [Range(1,200,ErrorMessage = "Title is not over 200 characters")]
        public string Title { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        [Range(500,5000,ErrorMessage = "Content between 500 to 5000 characters")]
        public string Text { get; set; }
    }
}
