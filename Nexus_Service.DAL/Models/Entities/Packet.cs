﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
   public class Packet
    {
        [Key]
        public int PacketId { get; set; }
        public string Name { get; set; }
        public bool Sattus { get; set; }
        public ICollection<Packet_Detail> packet_Details{ get; set; }
    }
}
