﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
   public class Device_Order
    {        
        [Key]
        public int BDid { get; set; }
        public int OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order order { get; set; }
        public int DeviceId { get; set; }
        [ForeignKey("DeviceId")]
        public virtual Device device { get; set; }
        public double Quantity { get; set; }
      
        public DateTime Date_create { get; set; }
        public double Total { get; set; }

     
        public Device_Order(int orderId, int deviceId, double quantity, DateTime date_create, double total)
        {
            OrderId = orderId;
            DeviceId = deviceId;
            Quantity = quantity;
            Date_create = date_create;
            Total = total;
        }

        public Device_Order()
        {
        }
    }
}
