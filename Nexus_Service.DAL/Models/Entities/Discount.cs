﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
    public class Discount
    {
        [Key]
        public int DiscountId { get; set; }
        [Required(ErrorMessage = "Please input a value")]
        [DataType(DataType.MultilineText)]
        public int value { get; set; } //value ở đây là % được chiết khấu, ví dụ như 25%, 50%...
                                       //đổi kiểu dữ liệu string thành int
       
    }
}
