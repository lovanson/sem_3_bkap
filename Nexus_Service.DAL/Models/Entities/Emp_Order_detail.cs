﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
   public class Emp_Order_detail
    {
        //bảng này là thực thể phụ
        [Key]
        public int EmpId { get; set; }
        [ForeignKey("EmpId")]
        public virtual Employee employee{ get; set; }
        public int OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order order { get; set; }
        public int? BillId { get; set; }
        [ForeignKey("BillId")]
        public virtual Bill Bill{ get; set; }
        public bool Status { get; set; }
        public DateTime date_create { get; set; }

        public Emp_Order_detail(int empId, int orderId, bool status, DateTime date_create)
        {
            EmpId = empId;
            OrderId = orderId;
            Status = status;
            this.date_create = date_create;
        }

       
        public Emp_Order_detail()
        {
        }
    }


}
