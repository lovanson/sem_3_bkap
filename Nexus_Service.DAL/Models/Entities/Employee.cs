﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
  public  class Employee
    {
        [Key]
        public int EmpId { get; set; }
        public string AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account account { get; set; }
        public int GroupId { get; set; }
        [ForeignKey("GroupId")]
        public virtual GroupUser groupUser{ get; set; }
      
        public bool Status { get; set; }
        [Required(ErrorMessage = "Please enter name")]
        public string Name { get; set; }
        [Required(ErrorMessage ="Please enter phone number")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        [Required(ErrorMessage ="Please enter email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter date of birth, age from 18 to 65")]
        [DoB(minAge = 18, maxAge = 65)]
        public DateTime DoB { get; set; }
        public ICollection<Emp_Order_detail> emp_Order_Details{ get; set; }
        public class DoBAttribute : ValidationAttribute
        {
            public int minAge { get; set; }
            public int maxAge { get; set; }
            public override bool IsValid(object value)
            {
                if (value == null)
                    return true;

                var val = (DateTime)value;

                if (val.AddYears(minAge) > DateTime.Now)
                    return false;

                return (val.AddYears(maxAge) > DateTime.Now);
            }
        }
    }
}
