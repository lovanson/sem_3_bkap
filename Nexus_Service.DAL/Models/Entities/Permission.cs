﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
   public class Permission
    {
        [Key]
        public int PermissionId { get; set; }
        public string Name { get; set; }
        public string Description{ get; set; }
        public string BusinessId { get; set; }
        [ForeignKey("BusinessId")]
        public Business Business { get; set; }
        public ICollection<GroupPermission> groupPermissions { get; set; }
    }
}
