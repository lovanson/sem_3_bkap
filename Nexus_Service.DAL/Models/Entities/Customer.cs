﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
  public class Customer
    {
        [Key]
        public int CusId { get; set; }
        public string AccountId { get; set; }
        [ForeignKey("AccountId")]
        public Account account { get; set; }

        public string Name { get; set; }
       
        public string Address { get; set; }
       
        public string Phone { get; set; }
     
        public string Email { get; set; }
        //DoB là date of birth - ngày tháng năm sinh
        //validate lấy ngày tháng năm, không lấy giờ phút
      
        public DateTime DoB { get; set; }
      
        public string IDcard { get; set; }
        
        public int CityId { get; set; }
        [ForeignKey("CityId")]
        public City city { get; set; }
        public bool Status{ get; set; }
        public ICollection<Order> orders { get; set; }
        public ICollection<Bill> bills{ get; set; }
     
    }
}
