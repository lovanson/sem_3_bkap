﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
    public class Category
    {       
        [Key]
        public int CatId { get; set; }
        [Required(ErrorMessage ="Please enter name")]
        public string CatName  { get; set; }
        public  ICollection<Service> services{ get; set; }
        public  ICollection<Device> devices{ get; set; }
}
}
