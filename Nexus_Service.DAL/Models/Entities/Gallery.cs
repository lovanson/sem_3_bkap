﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL.Models.Entities
{
    public class Gallery
    {
        [Key]
        public int GalId { get; set; }
        public string Image { get; set; }
    }
}
