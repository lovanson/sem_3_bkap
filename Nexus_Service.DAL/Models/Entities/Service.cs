﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
   public class Service
    {
        [Key]
        public int ServiceId { get; set; }
        [Required(ErrorMessage ="Please enter name")]
        public string Name { get; set; }
        public bool Status { get; set; }
        public int CatId { get; set; }
        [ForeignKey("CatId")]
        public virtual Category category { get; set; }
        public ICollection<Packet_Detail>  packet_Details{ get; set; }
    }
}
