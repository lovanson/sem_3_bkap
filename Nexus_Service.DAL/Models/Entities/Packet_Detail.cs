﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
    public class Packet_Detail
    {
        [Key]
        public int PDid { get; set; }
        public int PacketId { get; set; }
        [ForeignKey("PacketId")]
        public virtual Packet packet{ get; set; }
        public int ServiceId { get; set; }
        [ForeignKey("ServiceId")]
        public virtual Service service { get; set; }
        public string Name { get; set; }
        public string Tilte { get; set; }
        public string Detail { get; set; }
        public int DeviceId { get; set; }
        [ForeignKey("DeviceId")]
        public virtual Device device { get; set; }
        public int Effective_time { get; set; }
        public string  Image { get; set; }
       
        public double Price { get; set; }
        public object ImageName { get; set; }
        public object UploadImage { get; set; }

        public ICollection<Packet_Order> packet_Orders{ get; set; }
    }
}
