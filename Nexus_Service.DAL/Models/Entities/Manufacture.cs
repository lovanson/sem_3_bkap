﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
   public class Manufacture
    {
        [Key]
        public int ManuId { get; set; }
        [Required(ErrorMessage ="Please enter name")]
        public string Name { get; set; }
        public bool Status { get; set; }
        public ICollection<Device> devices { get; set; }
    }
}
