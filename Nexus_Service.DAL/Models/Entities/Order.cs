﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
    public class Order
    {
        [Key]
        public int OrderId { get; set; }
        public int CusId { get; set; }
        [ForeignKey("CusId")]
        public virtual Customer customer { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime DateDay { get; set; }
        public bool Status { get; set; }
        public int Quantity { get; set; }
        public double Total { get; set; }

        public ICollection<Packet_Order> packet_Orders { get; set; }
        public ICollection<Emp_Order_detail> emp_Order_Details{ get; set; }
        public ICollection<Device_Order> device_Orders { get; set; }

        public Order(bool status)
        {
            Status = status;
        }

        public Order()
        {
        }
    }
}
