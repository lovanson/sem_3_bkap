﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
    public class City
    {
        [Key]
        
        public int CityId { get; set; }
        [Required(ErrorMessage ="Please enter name")]
        [DataType(DataType.MultilineText)]
        public string Name { get; set; }
        public ICollection<Branch> branches { get; set; }
        public ICollection<Customer> customers{ get; set; }
    }
}
