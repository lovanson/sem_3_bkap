﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
    public class Customer_Service
    {
        [Key]
        public int CSid { get; set; }
        public int FeedbackId { get; set; }
        [ForeignKey("FeedbackId")]
        public Feedback feedback{ get; set; }
        public DateTime Date_rep { get; set; }
        public string Rep_content { get; set; }
        public int EmpId { get; set; }
        [ForeignKey("EmpId")]
        public Employee employee{ get; set; }
    }
}
