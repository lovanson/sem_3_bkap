﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
   public class Bill
    {
        [Key]
        public int BillId { get; set; }
       
        public int CusId { get; set; }
        [ForeignKey("CusId")]
        public virtual Customer customer { get; set; }
        [Required]
        public int Time_using { get; set; }
        public int Time_out { get; set; }
        public double Price_out { get; set; }
        public double Pay_more { get; set; }
        [Required]
        public double Total { get; set; }
        public DateTime Date_create { get; set; }
        public DateTime  Date_start_using { get; set; }
        public DateTime Date_end_using { get; set; }
        public int PayId { get; set; }
        [ForeignKey("PayId")]
        public virtual Payment_Method payment { get; set; }
        public bool Status { get; set; }

        public ICollection<Emp_Order_detail> emp_Order_Details{ get; set; }

        public Bill(int cusId, double total, DateTime date_create, DateTime date_start_using, DateTime date_end_using, int payId, bool status)
        {
            CusId = cusId;
            Total = total;
            Date_create = date_create;
            Date_start_using = date_start_using;
            Date_end_using = date_end_using;
            PayId = payId;
            Status = status;
        }

        public Bill()
        {
        }
    }
}
