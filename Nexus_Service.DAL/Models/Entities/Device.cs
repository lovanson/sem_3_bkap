﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
   public class Device
    {
        [Key]
        public int DeviceId { get; set; }
        public int? CatId { get; set; }
        [ForeignKey("CatId")]
        public virtual Category Category { get; set; }
        public int ManuId { get; set; }
        [ForeignKey("ManuId")]
        public virtual Manufacture manufacture { get; set; }
        [Required(ErrorMessage ="Please enter name")]
    
        public string Name { get; set; }
        [Required(ErrorMessage ="Please enter parameter")]
       
        public string Parameter { get; set; }
        [Required(ErrorMessage ="Please enter price")]
    
        public double Price { get; set; }
        [Required(ErrorMessage ="Please choose an image")]
     
        public string Image { get; set; }
        public bool Status { get; set; }
        public ICollection<Packet_Detail> packet_Details{ get; set; }
        public ICollection<Device_Order> device_Orders{ get; set; }


    }
}
