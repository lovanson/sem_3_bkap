﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
    public class Role_Assignment
    {
        [Key]
        public string AccountId { get; set; }
        [ForeignKey("AccountId")]
        public Account account { get; set; }
        public int PerId { get; set; }
        [ForeignKey("PerId")]
        public Permission permission { get; set; }
    }
}
