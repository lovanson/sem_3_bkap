﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
    public class Account
    {
        [Key]
        public string AccountId { get; set; }
        [Required(ErrorMessage = "LoginId must follow the pattern")]
        public string LoginId { get; set; }
        [Required]
        public string password { get; set; }
        [Required]
        public int level { get; set; }
        [Required]
        public int Role { get; set; }
        public DateTime Create_date { get; set; }
        public bool Status { get; set; }
    }
}
