﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nexus_Service.DAL
{
   public class Packet_Order
    {
        [Key]
        public int BPid { get; set; }
        public int PDid { get; set; }
        [ForeignKey("PDid")]
        public virtual Packet_Detail Packet_Detail{ get; set; }
        public int OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order Order{ get; set; }
        public bool Status{ get; set; }

        public Packet_Order(int pDid, int orderId, bool status)
        {
            PDid = pDid;
            OrderId = orderId;
            Status = status;
        }

        public Packet_Order()
        {
        }
    }
}
