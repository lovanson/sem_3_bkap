﻿using Nexus_Service.BLL;
using Nexus_Service.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Nexus_Service
{
    public class CustomizeAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            IRepository<Permission> permission = new Repository<Permission>();
            IRepository<GroupPermission> groupPermission = new Repository<GroupPermission>();
            IRepository<GroupUser> groupUser= new Repository<GroupUser>();
            IRepository<Employee> emply = new Repository<Employee>();
            if (httpContext.Session["Employee"] == null)
            {
                return false;
            }
            Employee em = (Employee)httpContext.Session["Employee"];
            if (groupUser.Get(em.GroupId).IsAdmin)
            {
                return true;
            }
            //xem nguoi dung truy cap chuc nang nao
            string ctrl =  httpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
          string act =  httpContext.Request.RequestContext.RouteData.Values["action"].ToString();
            string perName = ctrl + "Controller-" + act;
            Permission p = permission.Single(x => x.Name == perName);
            
            var grandet = groupPermission.Get(x => x.GroupId == em.GroupId);
            if (!grandet.Any(x => x.PermissionId == p.PermissionId))
            {
                return false;
            }
            return true;
        }
    }
    public class CustomeAuthenticationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            HttpSessionStateBase session = filterContext.HttpContext.Session;
            Controller controller = filterContext.Controller as Controller;

            if (controller != null)
            {
                if (session["Employee"] == null)
                {
                    //filterContext.Cancel = true;
                    //controller.HttpContext.Response.Redirect("/Admin/User/Login");
                    filterContext.Result = new RedirectToRouteResult(
                                           new RouteValueDictionary
                                           {
                                               { "action", "CookieLogin" },
                                               { "controller", "Users" }
                                           });
                }
                else
                {
                    return;

                }
            }

        }
    }

}