﻿using Nexus_Service.BLL;
using Nexus_Service.DAL;
using Nexus_Service.Models;
using Nexus_Service.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Nexus_Service.Controllers
{
    [RoutePrefix("Data")]
    public class DataController : ApiController
    {
        IRepository<Customer> _Customer;
        IRepository<Account> _Account;
        IRepository<Order> _Order;
        IRepository<Packet_Order> _PacketOrder;
        IRepository<Packet_Detail> _PacketDetail;
        private Connect_DB db = new Connect_DB();
        public DataController()
        {
            _Customer = new Repository<Customer>();
            _Account = new Repository<Account>();
            _Order = new Repository<Order>();
            _PacketOrder = new Repository<Packet_Order>();
            _PacketDetail = new Repository<Packet_Detail>();
        }
        [HttpPost]
        [Route("AddAccount/{idpket}")]
        public IHttpActionResult AddAccount(Account account, int idpket)
        {
            if (db.Accounts.Any(x => x.AccountId.Equals(account.AccountId)))
            {
                ModelState.AddModelError("CatName", "This name is exists");
            }
            if (ModelState.IsValid)
            {
                var accounts = _Account.Add(account);
                var idpk = idpket;
                var idAccount = account.AccountId;
                return Content(HttpStatusCode.OK, new
                {
                    message = "ok thanh công ",
                    data = accounts,
                    idpk = idpk,
                    idAccount = idAccount,
                    statusCode = 200
                });
            }
            return Content(HttpStatusCode.OK, new
            {
                message = "khong thanh cong ",

                statusCode = 200
            });
        }
        [HttpPost]
        [Route("AddCustomer")]
        public IHttpActionResult AddCustomer(Customer customer)
        {
            if (ModelState.IsValid)
            {
                var st = _Customer.Add(customer);

                var idcus = customer.CusId;
                return Content(HttpStatusCode.OK, new
                {
                    message = "Create Account Success ",
                    data = st,
                    idcs = idcus,
                    statusCode = 200
                });
            }
            return Content(HttpStatusCode.OK, new
            {
                message = "Error your Name or password ",

                statusCode = 200
            });
        }
        [HttpPost]
        [Route("AddOrder")]
        public IHttpActionResult AddOrder(Order order)
        {
            if (ModelState.IsValid)
            {
                var st = _Order.Add(order);

                var idorder = order.OrderId;
                return Content(HttpStatusCode.OK, new
                {
                    message = "Order Success and Waiting for a response from staff ",
                    data = st,
                    idorder = idorder,
                    statusCode = 200
                });

            }
            return Content(HttpStatusCode.OK, new
            {
                message = "Error",

                statusCode = 200
            });

        } 
       
        [HttpPost]
        [Route("AddPacketOrder")]
        public IHttpActionResult AddPacketOrder(Packet_Order packet_Order)
        {
            if (ModelState.IsValid)
            {
                var st = _PacketOrder.Add(packet_Order);
                return Content(HttpStatusCode.OK, new
                {
                    message = "ok thanh công ",
                    data = st,
                    statusCode = 200
                });
            }
            return Content(HttpStatusCode.OK, new
            {
                message = "khong thanh cong ",

                statusCode = 200
            });

        }
        [HttpPost]
        [Route("SendMailCus/{CusId}/{idpk}/{idtk}")]
        public IHttpActionResult SendMailCus(int CusId, int idpk ,string idtk)
        {
            var cus = _Customer.Get(CusId);
            var pk = _PacketDetail.Get(idpk);
                var body = "<h1>Order succes</h1>";
                body += "<b>Thank you </b>" + cus.Name + "<b> for choosing Nexus Service! </b> <br>";
                body += "<b>This is your order details </b> <br>";
                body += "<b>ID : </b>" + idtk + "<b>  </b>  <br>";
                body += "<b>Service : </b>" + pk.Name + "<b>  </b>  <br>";
                body += "<b>Total : </b>" + pk.Price + "<b>  </b>";
                SendMail.Send(cus.Email, "Nexus_Service", body);
                return Content(HttpStatusCode.OK, new
                {
                    message = "ok done ",
                    data = cus,
                    statusCode = 200
                }); 
          
        }

    }
}
