﻿using Nexus_Service.BLL;
using Nexus_Service.DAL;
using Nexus_Service.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Nexus_Service.Controllers
{
    public class ClineController : Controller
    {
        List<CartItem> carts = new List<CartItem>();
        private Connect_DB db = new Connect_DB();
        IRepository<Packet> _packet;
        IRepository<Packet_Detail> _packet_detail;
        IRepository<Category> _Category;
        IRepository<Service> _Service;
        IRepository<Device> _Device;
        IRepository<City> _City;
        IRepository<Customer> _Customer;
        IRepository<Order> _Order;
        IRepository<Account> _Account;
        IRepository<Device_Order> _DeviceOrder;
       
        public ClineController()
        {
            _packet_detail = new Repository<Packet_Detail>();
            _packet = new Repository<Packet>();
            _Category = new Repository<Category>();
            _Service = new Repository<Service>();
            _Device = new Repository<Device>();
            _City = new Repository<City>();
            _Customer = new Repository<Customer>();
            _Order = new Repository<Order>();
            _Account = new Repository<Account>();
            _DeviceOrder = new Repository<Device_Order>();
        }
        // GET: Cline
        public ActionResult TopMenu()
        {
            var ser = _Service.Get().ToList();
            ViewBag.ser = ser;
            return PartialView("_TopMenu");
        }
        public ActionResult Index()
        {

            return View(_packet_detail.Get().Take(3));
        }

        public ActionResult Contact()
        {
            return View();
        }
        public ActionResult Blog()
        {
            return View();
        }
        /* public ActionResult Packet(int? id)
         {
             if (id == null)
             {
                 return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
             }
            *//* var packet = (from packets in db.Packets
                           where  packets.PacketId ==id
                           select packets);*//*
             var pk = db.Packets.Where(x => x.ServiceId == id);

             return View(pk.AsEnumerable());

         }*/
        public ActionResult Services(int id , int page = 1, string search = "")
        {
            int pageSize = 3;
            var paket = db.Packet_Details.Where(x => x.ServiceId == id).AsEnumerable();
            ViewBag.totalPage = Math.Ceiling((decimal)paket.Count() / (decimal)pageSize);
            ViewBag.currentPage = page;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

         

            return View( paket.Skip((page - 1) * pageSize).Take(pageSize));
        }
        public ActionResult Shop(int page = 1, string search = "")
        {
            int pageSize = 3;
            var shop = _Device.Get().OrderBy(x => x.DeviceId).AsEnumerable();
            if (!String.IsNullOrEmpty(search))
            {
                shop = shop.Where(x => x.Name.ToLower().Contains(search.ToLower()));
                ViewBag.search = search;
            }
            ViewBag.totalPage = Math.Ceiling((decimal)shop.Count() / (decimal)pageSize);
            ViewBag.currentPage = page;
            return View(shop.Skip((page - 1) * pageSize).Take(pageSize));
        }
        public ActionResult Shop_detail(int id)
        {
            var shop = _Device.Get(id);
            ViewBag.DeviceId = _Device.Get().Where(x => x.DeviceId != id).Take(3).ToList();
            return View(shop);
        }
        public ActionResult Services_Detail(int id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.serverId = _packet_detail.Get().Where(x => x.PDid != id).Take(3).ToList();
            Packet_Detail packet_Detail = db.Packet_Details.Find(id);

            return View(packet_Detail);
        }
       
        public ActionResult AddToCart(int id_Device, int Quantity_Device)
        {
            CartItem c = new CartItem()
            {
              
                device = _Device.Get(id_Device),
                Quantity_device = Quantity_Device
            };
            if (Session["carts"] != null)
            {
                carts = (List<CartItem>)Session["carts"];
                bool flag = false;
                foreach (var item in carts)
                {
                   
                    if ( item.device.DeviceId == id_Device)
                    {
                        item.Quantity_device += Quantity_Device;
                        flag = true;
                        break;
                    }
                }

                if (!flag)
                {
                    carts.Add(c);
                }
            }
            else
            {
                carts.Add(c);
            }
            Session["carts"] = carts;
            return RedirectToAction("Cart");
        }
        public ActionResult add_order(Order order)
        {
            if (ModelState.IsValid)
            {
                carts = (List<CartItem>)Session["carts"];
                var st = _Order.SaveObject(order);
               
                var idorder = st.OrderId;
                var getdate = DateTime.Now.ToString("yyyy-MM-dd");
                foreach (var item in carts)
                {

                    var orderDetail = new Device_Order { OrderId = idorder, DeviceId = item.device.DeviceId, Quantity = item.Quantity_device, Date_create = Convert.ToDateTime(getdate), Total = item.Quantity_device * item.device.Price };
                    var dt = _DeviceOrder.Add(orderDetail);
                }
                return RedirectToAction("Index",new { messer = "Order Success and Waiting for a response from staff"});

            }
            return View();
        }
        public ActionResult SuaSoLuong(int DeviceId, int Quantity_devices)
        {
            // tìm carditem muon sua
            List<CartItem> carts = Session["carts"] as List<CartItem>;
            CartItem itemsua = carts.FirstOrDefault(m => m.device.DeviceId == DeviceId);
            if (itemsua != null)
            {
                itemsua.Quantity_device = Quantity_devices;
            }
            return RedirectToAction("Cart");

        }
        public ActionResult Cart()
        {
            List<CartItem> carts = new List<CartItem>();
            if (Session["carts"] != null)
            {
                carts = (List<CartItem>)Session["carts"];
            }
            return View(carts);
        }
        public ActionResult DeleteCart(int DeviceId)
        {
            List<CartItem> carts = Session["carts"] as List<CartItem>;
            CartItem itemXoa = carts.FirstOrDefault(m => m.device.DeviceId == DeviceId);
            if (itemXoa != null)
            {
                carts.Remove(itemXoa);
            }
            return RedirectToAction("Cart");
        }
        public ActionResult CheckOutDevice()
        {
            List<CartItem> carts = new List<CartItem>();
            if (Session["carts"] != null)
            {
                carts = (List<CartItem>)Session["carts"];
            }
            return View(carts);
        }
        public ActionResult CheckOutCity(int id)
        {
            var pk = _packet_detail.Get(id);
           
                if (pk.service.Name.ToString() == "Internet")
                {
                    ViewBag.getId = "I";
                }
                else if (pk.service.Name.ToString() == "Dial-Up")
                {
                    ViewBag.getId = "D";
                }

           
            ViewBag.id = id;
            List<City> ct = db.Cities.ToList();
            SelectList list = new SelectList(_City.Get(), "CityId", "Name");
            ViewBag.CityId = list;
            return View();
        }
       
        public ActionResult CheckOut(int id, string name, int CityId )
        {
            var id_accout = name + CityId ;
            ViewBag.id = id_accout;
            ViewBag.idpk = id;
            return View();
        }
        [HttpGet]
        [Route("CheckOutDt/{idpk}/{idAccount}")]
        public ActionResult CheckOutDt( int idpk , string idAccount)
        {
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "Name");
            ViewBag.acid = idAccount;
            ViewBag.idpk = idpk;
            var price = _packet_detail.Get(idpk);
            ViewBag.pr = price.Price;
            var loginID = _Account.Get(idAccount).LoginId;
            ViewBag.loginID = loginID;
            return View("CheckOutDt");
        }
      
       
       
       

    }
}
