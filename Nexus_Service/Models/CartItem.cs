﻿using Nexus_Service.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nexus_Service.Models
{
    public class CartItem
    {
        public Packet_Detail Packet_Detail{ get; set; }
        public int Quantity_pk { get; set; }
        public Device  device{ get; set; }
        public int Quantity_device { get; set; }
    }
}