﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nexus_Service.Areas.Admin.Data
{
    public class GrandPermission
    {
        public int PermissionId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string BusinessId { get; set; }
        public bool IsGrandted { get; set; }
    }
}