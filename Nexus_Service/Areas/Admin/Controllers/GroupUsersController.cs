﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nexus_Service.Areas.Admin.Data;
using Nexus_Service.BLL;
using Nexus_Service.DAL;

namespace Nexus_Service.Areas.Admin.Controllers
{
    [CustomizeAuthorize]
    public class GroupUsersController : Controller
    {
        IRepository<Business> businesses;
        IRepository<GroupUser> groupUser;
        IRepository<Permission> permission;
        IRepository<GroupPermission> groupPermission;
        
        private Connect_DB db = new Connect_DB();
        public GroupUsersController()
        {
            businesses = new Repository<Business>();
            groupUser = new Repository<GroupUser>();
            permission = new Repository<Permission>();
            groupPermission = new Repository<GroupPermission>();
        }


        // GET: Admin/GroupUsers
        public ActionResult Index()
        {
            return View(db.GroupUsers.ToList());
        }

        // GET: Admin/GroupUsers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GroupUser groupUser = db.GroupUsers.Find(id);
            if (groupUser == null)
            {
                return HttpNotFound();
            }
            return View(groupUser);
        }

        // GET: Admin/GroupUsers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/GroupUsers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GroupName,IsAdmin,Status")] GroupUser groupUser)
        {
            if (ModelState.IsValid)
            {
                db.GroupUsers.Add(groupUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(groupUser);
        }

        // GET: Admin/GroupUsers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GroupUser groupUser = db.GroupUsers.Find(id);
            if (groupUser == null)
            {
                return HttpNotFound();
            }
            return View(groupUser);
        }

        // POST: Admin/GroupUsers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,GroupName,IsAdmin,Status")] GroupUser groupUser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(groupUser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(groupUser);
        }

        // GET: Admin/GroupUsers/Delete/5
        public ActionResult Delete(int id)
        {
            groupUser.Remove(id);
            return RedirectToAction("Index");
        }

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult GrandPermission(int id)
        {
            ViewBag.BusinessId = new SelectList(businesses.Get(), "BusinessId", "Name");
            return View(groupUser.Get(id));
        }
        public ActionResult GetPermission(string businessId ,int groupId)
        {
            var grandPermission = permission.Get(x => x.BusinessId.Equals(businessId))
                .Select(x => new GrandPermission
                {
                    BusinessId = x.BusinessId,
                    Description = x.Description,
                    IsGrandted = false,
                    Name = x.Name,
                    PermissionId = x.PermissionId
                }).ToList();
            foreach (var item in grandPermission)
            {
                if (groupPermission.Any(x =>  x.GroupId ==groupId && x.PermissionId == item.PermissionId))
                {
                    item.IsGrandted = true;
                }
            }
            return Json(grandPermission, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ChangePermission(int groupId, int perId)
        {
            var gr = groupPermission.Single(x => x.GroupId == groupId && x.PermissionId == perId);
            string mes = "";
            if (gr != null)
            {
                groupPermission.Remove(gr.Id);
                mes = " huy quyen thanh conh";
            }
            else
            {
                mes = " gan quyen  thanh cong!";
                groupPermission.Add(new GroupPermission { GroupId = groupId, PermissionId = perId });
            }
            return Json( new { messger = mes},JsonRequestBehavior.AllowGet);
        }
    }
}
