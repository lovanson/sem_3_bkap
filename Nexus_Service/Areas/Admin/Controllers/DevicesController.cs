﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nexus_Service.BLL;
using Nexus_Service.DAL;

namespace Nexus_Service.Areas.Admin.Controllers
{
    [CustomizeAuthorize]
    public class DevicesController : Controller
    {
        private Connect_DB db = new Connect_DB();

        IRepository<Device> _Device;
        public DevicesController()
        {
            _Device = new Repository<Device>();
        }
        // GET: Admin/Devices
        public ActionResult Index()
        {
            var devices = db.Devices.Include(d => d.Category).Include(d => d.manufacture);
            return View(devices.ToList());
        }

        // GET: Admin/Devices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        // GET: Admin/Devices/Create
        public ActionResult Create()
        {
            ViewBag.CatId = new SelectList(db.Categories, "CatId", "CatName");
            ViewBag.ManuId = new SelectList(db.Manufactures, "ManuId", "Name");
            return View();
        }

        // POST: Admin/Devices/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DeviceId,CatId,ManuId,Name,Parameter,Price,Image,Status")] Device device)
        {
            if (db.Devices.Any(x => x.Name.Equals(device.Name)))
            {
                ModelState.AddModelError("Name", "This name is exists");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    HttpPostedFileBase file = Request.Files[0];
                    string FileName = file.FileName;
                    device.Image = FileName;
                    string pash = Path.Combine(Server.MapPath("~/Content/UploadFile/Device"), FileName);
                    file.SaveAs(pash);

                    db.Devices.Add(device);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                    ViewBag.Message = "Upload File successfully!";
                }
                catch (Exception)
                {

                    ViewBag.Message = "Upload File false!";
                }
              
            }
            ViewBag.CatId = new SelectList(db.Categories, "CatId", "CatName");
            ViewBag.ManuId = new SelectList(db.Manufactures, "ManuId", "Name");
            return View(device);
        }

        // GET: Admin/Devices/Edit/5
     /*   public string uploadimage(HttpPostedFileBase file)
        {
            Random r = new Random();
            string path = "-1";
            int random = r.Next();
            if (file != null && file.ContentLength > 0)
            {
                string extension = Path.GetExtension(file.FileName);
                if (extension.ToLower().Equals(".jpg") || extension.ToLower().Equals(".jpeg") || extension.ToLower().Equals(".png"))
                {
                    try
                    {
                        path = Path.Combine(Server.MapPath("~/Content/Images"), random + Path.GetFileName(file.FileName));
                        file.SaveAs(path);
                        path = "~/Content/UploadFile/Device" + random + Path.GetFileName(file.FileName);
                    }
                    catch (Exception ex)
                    {
                        path = "-1";
                    }
                }
                else
                {
                    Response.Write("<script>alert('Only jpg ,jpeg or png formats are acceptable....'); </script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Please select a file'); </script>");
                path = "-1";
            }
            return path;
        }*/
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            ViewBag.CatId = new SelectList(db.Categories, "CatId", "CatName", device.CatId);
            ViewBag.ManuId = new SelectList(db.Manufactures, "ManuId", "Name", device.ManuId);
            return View(device);
        }

        // POST: Admin/Devices/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DeviceId,CatId,ManuId,Name,Parameter,Price,Image,Status")] Device device)
        {
            if (db.Devices.Any(x => x.Name.Equals(device.Name) && x.DeviceId != (device.DeviceId)))
            {
                ModelState.AddModelError("Name", "This name is exists");
            }
            if (ModelState.IsValid)
            {
                try
                {

                    HttpPostedFileBase file = Request.Files[0];
                    string FileName = file.FileName;
                    device.Image = FileName;
                    string pash = Path.Combine(Server.MapPath("~/Content/UploadFile/Device"), FileName);
                    file.SaveAs(pash);
                    var devs = db.Devices.Find(device.DeviceId);
                    devs.Status = device.Status;
                    devs.Name = device.Name;
                    devs.ManuId = device.ManuId;
                    devs.CatId = device.CatId;
                    devs.Image = device.Image;
                    devs.Parameter = device.Parameter;
                    devs.Price = device.Price;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {


                    ViewBag.Message = "Upload File false!";
                }
              
            }
            ViewBag.CatId = new SelectList(db.Categories, "CatId", "CatName", device.CatId);
            ViewBag.ManuId = new SelectList(db.Manufactures, "ManuId", "Name", device.ManuId);
            return View(device);
        }

        // GET: Admin/Devices/Delete/5
        public ActionResult Delete(int id)
        {
            _Device.Remove(id);
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
