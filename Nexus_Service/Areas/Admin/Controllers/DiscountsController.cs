﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nexus_Service.BLL;
using Nexus_Service.DAL;

namespace Nexus_Service.Areas.Admin.Controllers
{
    [CustomizeAuthorize]
    public class DiscountsController : Controller
    {
        IRepository<Discount> _discount;
        private Connect_DB db = new Connect_DB();

        // GET: Admin/Discounts
        public DiscountsController()
        {
            _discount = new Repository<Discount>();
        }
        public ActionResult Index()
        {
            return View(db.Discounts.ToList());
        }

        // GET: Admin/Discounts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discount discount = db.Discounts.Find(id);
            if (discount == null)
            {
                return HttpNotFound();
            }
            return View(discount);
        }

        // GET: Admin/Discounts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Discounts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DiscountId,value")] Discount discount)
        {
            if (db.Discounts.Any(x => x.value.Equals(discount.value)))
            {
                ModelState.AddModelError("value", "This value is exists");
            }
            if (ModelState.IsValid)
            {
                db.Discounts.Add(discount);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(discount);
        }

        // GET: Admin/Discounts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discount discount = db.Discounts.Find(id);
            if (discount == null)
            {
                return HttpNotFound();
            }
            return View(discount);
        }

        // POST: Admin/Discounts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DiscountId,value")] Discount discount)
        {
            if (db.Discounts.Any(x => x.value.Equals(discount.value) && x.DiscountId != (discount.DiscountId)))
            {
                ModelState.AddModelError("value", "This value is exists");
            }
            if (ModelState.IsValid)
            {
                var disc = db.Discounts.Find(discount.DiscountId);
                disc.value = discount.value;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(discount);
        }

        // GET: Admin/Discounts/Delete/5
        public ActionResult Delete(int id)
        {
            _discount.Remove(id);
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
