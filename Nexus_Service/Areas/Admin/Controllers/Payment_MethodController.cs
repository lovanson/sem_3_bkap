﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nexus_Service.BLL;
using Nexus_Service.DAL;

namespace Nexus_Service.Areas.Admin.Controllers
{
    [CustomizeAuthorize]
    public class Payment_MethodController : Controller
    {
        private Connect_DB db = new Connect_DB();
        IRepository<Payment_Method> _payment;
        public Payment_MethodController()
        {
            _payment = new Repository<Payment_Method>();
        }

        // GET: Admin/Payment_Method
        public ActionResult Index()
        {
            return View(db.Payment_Methods.ToList());
        }

        // GET: Admin/Payment_Method/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payment_Method payment_Method = db.Payment_Methods.Find(id);
            if (payment_Method == null)
            {
                return HttpNotFound();
            }
            return View(payment_Method);
        }

        // GET: Admin/Payment_Method/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Payment_Method/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PayId,Type,Status")] Payment_Method payment_Method)
        {
            if (db.Payment_Methods.Any(x => x.Type.Equals(payment_Method.Type)))
            {
                ModelState.AddModelError("Type", "This type is exists");
            }
            if (ModelState.IsValid)
            {
                db.Payment_Methods.Add(payment_Method);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payment_Method);
        }

        // GET: Admin/Payment_Method/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payment_Method payment_Method = db.Payment_Methods.Find(id);
            if (payment_Method == null)
            {
                return HttpNotFound();
            }
            return View(payment_Method);
        }

        // POST: Admin/Payment_Method/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PayId,Type,Status")] Payment_Method payment_Method)
        {
           if (db.Payment_Methods.Any(x => x.Type.Equals(payment_Method.Type) && x.PayId != (payment_Method.PayId)))
            {
                ModelState.AddModelError("Type", "This type is exists");
            }
            if (ModelState.IsValid)
            {
                var paym = db.Payment_Methods.Find(payment_Method.PayId);
                paym.Status = payment_Method.Status;
                paym.Type = payment_Method.Type;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payment_Method);
        }

        // GET: Admin/Payment_Method/Delete/5
        public ActionResult Delete(int id)
        {
            _payment.Remove(id);
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
