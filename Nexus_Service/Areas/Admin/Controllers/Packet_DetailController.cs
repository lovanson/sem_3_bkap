﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nexus_Service.BLL;
using Nexus_Service.DAL;

namespace Nexus_Service.Areas.Admin.Controllers
{
    [CustomizeAuthorize]
    public class Packet_DetailController : Controller
    {
        private Connect_DB db = new Connect_DB();
        IRepository<Packet_Detail> _packet_detai;
        public Packet_DetailController()
        {
            _packet_detai = new Repository<Packet_Detail>();
        }
        // GET: Admin/Packet_Detail
        public ActionResult Index()
        {
            var packet_Details = db.Packet_Details.Include(p => p.device).Include(p => p.packet).Include(p=>p.service);
            return View(packet_Details.ToList());
        }

        // GET: Admin/Packet_Detail/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Packet_Detail packet_Detail = db.Packet_Details.Find(id);
            if (packet_Detail == null)
            {
                return HttpNotFound();
            }
            return View(packet_Detail);
        }

        // GET: Admin/Packet_Detail/Create
        public ActionResult Create()
        {
            ViewBag.DeviceId = new SelectList(db.Devices, "DeviceId", "Name");
            ViewBag.PacketId = new SelectList(db.Packets, "PacketId", "Name");
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "Name");
            return View();
        }

        // POST: Admin/Packet_Detail/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PDid,PacketId,ServiceId,Name,Tilte,Detail,DeviceId,Effective_time,Image,Price")] Packet_Detail packet_Detail)
        {
            if (ModelState.IsValid)
            {
                
                try
                {
                    HttpPostedFileBase file = Request.Files[0];
                    string FileName = file.FileName;
                    packet_Detail.Image = FileName;
                    string pash = Path.Combine(Server.MapPath("~/Content/UploadFile/Packet_detail"), FileName);
                    file.SaveAs(pash);
              
                    db.Packet_Details.Add(packet_Detail);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                    ViewBag.Message = "Upload File successfully!";
                }
                catch (Exception)
                {

                    ViewBag.Message = "Upload File false!";
                }
              
            }

            ViewBag.DeviceId = new SelectList(db.Devices, "DeviceId", "Name", packet_Detail.DeviceId);
            ViewBag.PacketId = new SelectList(db.Packets, "PacketId", "Name", packet_Detail.PacketId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "Name",packet_Detail.ServiceId);

            return View(packet_Detail);
        }

        // GET: Admin/Packet_Detail/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Packet_Detail packet_Detail = db.Packet_Details.Find(id);
            if (packet_Detail == null)
            {
                return HttpNotFound();
            }
            ViewBag.DeviceId = new SelectList(db.Devices, "DeviceId", "Name", packet_Detail.DeviceId);
            ViewBag.PacketId = new SelectList(db.Packets, "PacketId", "Name", packet_Detail.PacketId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "Name", packet_Detail.ServiceId);
            return View(packet_Detail);
        }

        // POST: Admin/Packet_Detail/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PDid,PacketId,ServiceId,Name,Tilte,Detail,DeviceId,Effective_time,Image,Price")] Packet_Detail packet_Detail)
        {
            if (ModelState.IsValid)
            {

                try
                {
                    HttpPostedFileBase file = Request.Files[0];
                    string FileName = file.FileName;
                    packet_Detail.Image = FileName;
                    string pash = Path.Combine(Server.MapPath("~/Content/UploadFile/Packet_detail"), FileName);
                    file.SaveAs(pash);
                    db.Entry(packet_Detail).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {

                    ViewBag.Message = "Upload File false!";
                }
             
            }
            ViewBag.DeviceId = new SelectList(db.Devices, "DeviceId", "Name", packet_Detail.DeviceId);
            ViewBag.PacketId = new SelectList(db.Packets, "PacketId", "Name", packet_Detail.PacketId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "Name", packet_Detail.ServiceId);
            return View(packet_Detail);
        }

        // GET: Admin/Packet_Detail/Delete/5
        public ActionResult Delete(int id)
        {
            _packet_detai.Remove(id);
            return RedirectToAction("Index");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
