﻿using Nexus_Service.BLL;
using Nexus_Service.DAL;
using Nexus_Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nexus_Service.Areas.Admin.Controllers
{
    [CustomizeAuthorize]
    public class BusinessesController : Controller
    {
        private IRepository<Business> _business;
        private IRepository<Permission> _Permission;
        public BusinessesController()
        {
            _business = new Repository<Business>();
            _Permission = new Repository<Permission>();
        }
        // GET: Admin/Businesses
        public ActionResult Index()
        {
            return View(_business.Get());
        }
        public ActionResult Update()
        {
            var controller = Helper.GetControllers("Nexus_Service.Areas.Admin.Controllers");
            foreach (var item in controller)
            {
                Business b = new Business
                {
                    BusinessId = item.Name,
                    Name = item.Name
                };
                if (!_Permission.Any(x => x.BusinessId.Equals(b.BusinessId)))
                {
                    _business.Add(b);

                }
                var acts = Helper.GetActions(item);
                foreach (var act in acts)
                {
                    Permission p = new Permission
                    {
                        BusinessId = item.Name,
                        Name = item.Name + "-" + act,
                        Description = item.Name + "-" + act
                    };
                    if (!_Permission.Any(x => x.Name.Equals(p.Name)))
                    {
                        _Permission.Add(p);
                    }
                }
            }
           
            return RedirectToAction("Index");
        }
    }
}