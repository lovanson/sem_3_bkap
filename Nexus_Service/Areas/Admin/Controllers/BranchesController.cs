﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nexus_Service.BLL;
using Nexus_Service.DAL;

namespace Nexus_Service.Areas.Admin.Controllers
{
    [CustomizeAuthorize]
    public class BranchesController : Controller
    {
        private Connect_DB db = new Connect_DB();
        IRepository<Branch> _branch;
        IRepository<City> _city;
        // GET: Admin/Branches
        public BranchesController()
        {
            _branch = new Repository<Branch>();
            _city = new Repository<City>();
        }
        public ActionResult Index()
        {
            var branches = db.Branches.Include(b => b.city);
            return View(branches.ToList());
        }

        // GET: Admin/Branches/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Branch branch = db.Branches.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            return View(branch);
        }

        // GET: Admin/Branches/Create
        public ActionResult Create()
        {
            ViewBag.CityId = new SelectList(_city.Get(), "CityId", "Name").ToList();
            return View();
        }

        // POST: Admin/Branches/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BranchId,CityId,Name,Phone,Status")] Branch branch)
        {
            if (db.Branches.Any(x => x.Phone.Equals(branch.Phone)))
            {
                ModelState.AddModelError("Phone", "This Phone is exists");
            }
            if (ModelState.IsValid)
            {
                db.Branches.Add(branch);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CityId = new SelectList(_city.Get(), "CityId", "Name").ToList();
            return View(branch);
        }

        // GET: Admin/Branches/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Branch branch = db.Branches.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "Name", branch.CityId);
            return View(branch);
        }

        // POST: Admin/Branches/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BranchId,CityId,Name,Phone,Status")] Branch branch)
        {
            if (db.Branches.Any(x => x.Phone.Equals(branch.Phone) && x.BranchId != (branch.BranchId)))
            {
                ModelState.AddModelError("Phone", "This Phone is exists");
            }
            if (ModelState.IsValid)
            {
                var branches = db.Branches.Find(branch.BranchId);
                branches.Status = branch.Status;
                branches.Name = branch.Name;
                branches.Phone = branch.Phone;
                branches.CityId = branch.CityId;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CityId = new SelectList(_city.Get(), "CityId", "Name").ToList();
            return View(branch);
        }

        // GET: Admin/Branches/Delete/5
        public ActionResult Delete(int id)
        {
            _branch.Remove(id);
            return RedirectToAction("Index");
        }

      

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
