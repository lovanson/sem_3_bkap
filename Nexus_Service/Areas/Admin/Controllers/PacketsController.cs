﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nexus_Service.BLL;
using Nexus_Service.DAL;

namespace Nexus_Service.Areas.Admin.Controllers
{
    [CustomizeAuthorize]
    public class PacketsController : Controller
    {
        private Connect_DB db = new Connect_DB();
        IRepository<Packet> pac;
        // GET: Admin/Packets
        public PacketsController()
        {
            pac = new Repository<Packet>();
        }
        public ActionResult Index()
        {
            var packets = pac.Get();
            return View(packets.ToList());
        }

        // GET: Admin/Packets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Packet packet = db.Packets.Find(id);
            if (packet == null)
            {
                return HttpNotFound();
            }
            return View(packet);
        }

        // GET: Admin/Packets/Create
        public ActionResult Create()
        {
         
            return View();
        }

        // POST: Admin/Packets/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PacketId,Name,Sattus")] Packet packet)
        {

            if (ModelState.IsValid)
            {
                db.Packets.Add(packet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

         
            return View(packet);
        }

        // GET: Admin/Packets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Packet packet = db.Packets.Find(id);
            if (packet == null)
            {
                return HttpNotFound();
            }
           
            return View(packet);
        }

        // POST: Admin/Packets/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PacketId,Name,Sattus")] Packet packet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(packet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(packet);
        }

        // GET: Admin/Packets/Delete/5
        public ActionResult Delete(int id)
        {
            pac.Remove(id);
            return RedirectToAction("Index");
        }

      
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
