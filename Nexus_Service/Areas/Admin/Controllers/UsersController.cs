﻿using Nexus_Service.BLL;
using Nexus_Service.DAL;
using Nexus_Service.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Nexus_Service.Areas.Admin.Controllers
{
    public class UsersController : Controller
    {
        // GET: Admin/Users
        IRepository<Account> _Account;
        IRepository<Employee> _Employee;
        IRepository<Order> _Order;
        IRepository<Device_Order> _DeviceOrder;
        IRepository<Device> _Device;
        IRepository<Emp_Order_detail> _Emp_Order_detail;
        IRepository<Payment_Method> _Payment_Method;
        IRepository<Bill> _Bill;
        private Connect_DB db = new Connect_DB();

        public UsersController()
        {
            _Account = new Repository<Account>();
            _Employee = new Repository<Employee>();
            _Order = new Repository<Order>();
            _DeviceOrder = new Repository<Device_Order>();
            _Device = new Repository<Device>();
            _Emp_Order_detail = new Repository<Emp_Order_detail>();
            _Payment_Method = new Repository<Payment_Method>();
            _Bill = new Repository<Bill>();
        }
        [CustomizeAuthorize]
        public ActionResult TopMenu()
        {

            ViewBag.userName = Session["Employee"];
            
            return PartialView("_TopMenu");
        }
        [CustomizeAuthorize]
        public ActionResult Index()
        {
            if (Session["Employee"] == null)
            {
                return RedirectToAction("CookieLogin");
            }
          //  ViewBag.userName = Request.Cookies["UserName"].Value;
            //ViewBag.Id = Request.Cookies["Id"].Value;
            return View();
        }
        public static string GetMD5(string str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = md5.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }
        [AllowAnonymous]
        public ActionResult CookieLogin()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult CookieLogin(string LoginId, string password)
        {
            var acc = _Account.Get();

            foreach (var item in acc)
            {
                var empl = _Employee.Get().Where(x => x.AccountId == item.AccountId).ToList();
                var f_password = GetMD5(password);
                foreach (var em in empl)
                {
                    var name = em.Name.ToString();
                    var employee = _Employee.Get(em.EmpId);
                    if (LoginId == item.LoginId && item.password == f_password && item.level != 3)
                    {
                        Session["Employee"] = employee;
                        return RedirectToAction("Index");
                    }
                }

            }
            return View();

        }
        public ActionResult CookieLogout()
        {
            Session["Employee"] = null;
            return RedirectToAction("CookieLogin");
        }
        [CustomizeAuthorize]
        public ActionResult OrderNew()
        {
            if (Session["Employee"] == null)
            {
                return RedirectToAction("CookieLogin");
            }
            var cusid = 2;
            var order = _Order.Get().Where(x => x.CusId == cusid).ToList();
            var dd = order.Where(x => x.Status == true).ToList();
            return View(dd);
        }
        [CustomizeAuthorize]
        public ActionResult OrderDone()
        {
            if (Session["Employee"] == null)
            {
                return RedirectToAction("CookieLogin");
            }
            var cusid = 2;
            var order = _Order.Get().Where(x => x.CusId == cusid).ToList();
            var dd = order.Where(x => x.Status == false).ToList();
            return View(dd);
        }
        [CustomizeAuthorize]
        public ActionResult OrderDoneDt(int id)
        {
            if (Session["Employee"] == null)
            {
                return RedirectToAction("CookieLogin");
            }
            ViewBag.order = _Order.Get(id);
            ViewBag.od = _Order.Get(id);
            var idEmpoyee = Request.Cookies["Id"].Value;
            var idEp = db.Employees.FirstOrDefault(x => x.EmpId.ToString() == idEmpoyee);
            ViewBag.idEp = idEp.EmpId;
            var _dvOrder = db.Device_Orders.Where(x => x.OrderId == id).ToList();

            return View(_dvOrder);
        }
        [CustomizeAuthorize]
        public ActionResult DeleteOrderDv(int id)
        {
            _Order.Remove(id);
            return RedirectToAction("OrderNew");
        }
        [CustomizeAuthorize]
        public ActionResult OrderNewDt(int id)
        {
            if (Session["Employee"] == null)
            {
                return RedirectToAction("CookieLogin");
            }
            ViewBag.order = _Order.Get(id);

            //var _dvOrder = (from _do i    n db.Device_Orders
            //                join
            //                    _d in db.Devices
            //                    on _do.DeviceId equals _d.DeviceId
            //                select new {  }).ToList();
            //var _dvOrder = db.Device_Orders.Join(db.Devices, x => x.DeviceId, y => y.DeviceId, (x, y) => x).ToList();
            ViewBag.od = _Order.Get(id);
            var idac = Request.Cookies["Id"].Value;
            var idEp = db.Employees.FirstOrDefault(x => x.EmpId.ToString() == idac);
            ViewBag.idEp = idEp.EmpId;
            var _dvOrder = db.Device_Orders.Where(x => x.OrderId == id ).ToList();
            
            return View(_dvOrder);
        }
        [CustomizeAuthorize]
        public ActionResult BrowseDevice( Emp_Order_detail emp_Order_Detail)
        {
            if (ModelState.IsValid)
            {
                var emOd = _Emp_Order_detail.Add(emp_Order_Detail);
                bool status = false;
                var orId = emp_Order_Detail.OrderId;
                var od = _Order.Get(orId);
                od.Status = status;
                var ord = _Order.Edit(od);
               
                db.SaveChanges();
                return RedirectToAction("Index");
            }
                return View();
        }
        [CustomizeAuthorize]
        public ActionResult SendMailCheckDevice(int OrderId)
        {
            var od = _Order.Get(OrderId);
            var _dvOrder = db.Device_Orders.Where(x => x.OrderId == OrderId).ToList();
            var body = "<h1>Order Device</h1>";
            body += "<b>Thank you for choosing Nexus Service! </b> <br>";
            body += "<b>Your order has been approved and the product will be shipped soon. </b>" + od.Address + "<b> <br>";
            body += "<b>Total price is : </b>" + od.Total + "<b>$  </b>  <br>";
            body += "<b>Include  : </b>" + od.Quantity + "<b> devices</b> <br>";
         
            SendMail.Send(od.Email, "Nexus_Service", body);
            return RedirectToAction("Index");

        }
        //order service 
        [CustomizeAuthorize]
        public ActionResult OrderSerciceNew()
        {
            if (Request.Cookies["UserName"] == null)
            {
                return RedirectToAction("CookieLogin");
            }
            var cusid = 2;
            var order = _Order.Get().Where(x => x.CusId != cusid).ToList();
            var dd = order.Where(x => x.Status == true).ToList();
            return View(dd);
        }
        [CustomizeAuthorize]
        public ActionResult OrderSerciceNewDt(int id)
        {
            if (Request.Cookies["UserName"] == null)
            {
                return RedirectToAction("CookieLogin");
            }
            List<City> ct = db.Cities.ToList();
            SelectList list = new SelectList(_Payment_Method.Get(), "PayId", "Type", "Status");
            ViewBag.PayId = list;
            ViewBag.order = _Order.Get(id);
            ViewBag.od = _Order.Get(id);
            var idac = Request.Cookies["Id"].Value;
            var idEp = db.Employees.FirstOrDefault(x => x.EmpId.ToString() == idac);
            ViewBag.idEp = idEp.EmpId;
            var _pkOrder = db.Packet_Orders.Where(x => x.OrderId == id);

            return View(_pkOrder);
        }
        [CustomizeAuthorize]
        public ActionResult OrderServiceBrowsing()
        {
            if (Request.Cookies["UserName"] == null)
            {
                return RedirectToAction("CookieLogin");
            }
            var cusid = 2;
            var order = _Order.Get().Where(x => x.CusId != cusid).ToList();
            var dd = order.Where(x => x.Status == false).ToList();
            return View(dd);
        }
        [CustomizeAuthorize]
        public ActionResult OrderServiceBrowsingDt(int id)
        {
            if (Request.Cookies["UserName"] == null)
            {
                return RedirectToAction("CookieLogin");
            }
            var order = _Order.Get(id);
            var bll = _Bill.Get().FirstOrDefault(x => x.CusId == order.CusId);
            ViewBag.bll = bll;
            ViewBag.od = _Order.Get(id);
            var idac = Request.Cookies["Id"].Value;
            var idEp = db.Employees.FirstOrDefault(x => x.EmpId.ToString() == idac);
            ViewBag.idEp = idEp.EmpId;
            var _pkOrder = db.Packet_Orders.Where(x => x.OrderId == id);

            return View(_pkOrder);
        }
        [CustomizeAuthorize]
        public ActionResult AddBill(Bill bill)
        {
            if (ModelState.IsValid)
            {
                var bll = _Bill.Add(bill);
                return RedirectToAction("OrderSerciceNew");
            }
            return View();
         }
    }
}