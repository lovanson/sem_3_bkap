﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nexus_Service.BLL;
using Nexus_Service.DAL;

namespace Nexus_Service.Areas.Admin.Controllers
{
    [CustomizeAuthorize]
    public class ManufacturesController : Controller
    {
        private Connect_DB db = new Connect_DB();
        IRepository<Manufacture> manufacture;
        public ManufacturesController()
        {
            manufacture = new Repository<Manufacture>();
        }
        // GET: Admin/Categories
        // GET: Admin/Manufactures
        public ActionResult Index()
        {
            return View(db.Manufactures.ToList());
        }

        // GET: Admin/Manufactures/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Manufacture manufacture = db.Manufactures.Find(id);
            if (manufacture == null)
            {
                return HttpNotFound();
            }
            return View(manufacture);
        }

        // GET: Admin/Manufactures/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Manufactures/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ManuId,Name,Status")] Manufacture manufacture)
        {
            if (db.Manufactures.Any(x => x.Name.Equals(manufacture.Name)))
            {
                ModelState.AddModelError("Name", "This name is exists");
            }
            if (ModelState.IsValid)
            {
                db.Manufactures.Add(manufacture);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(manufacture);
        }

        // GET: Admin/Manufactures/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Manufacture manufacture = db.Manufactures.Find(id);
            if (manufacture == null)
            {
                return HttpNotFound();
            }
            return View(manufacture);
        }

        // POST: Admin/Manufactures/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ManuId,Name,Status")] Manufacture manufacture)
        {
            if (db.Manufactures.Any(x => x.Name.Equals(manufacture.Name) && x.ManuId != (manufacture.ManuId)))
            {
                ModelState.AddModelError("Name", "This name is exists");
            }
            if (ModelState.IsValid)
            {
                var manu = db.Manufactures.Find(manufacture.ManuId);
                manu.Status = manufacture.Status;
                manu.Name = manufacture.Name;
                db.SaveChanges();
                return RedirectToAction("Index");
            }           
            return View(manufacture);
        }

        // GET: Admin/Manufactures/Delete/5
        public ActionResult Delete(int id)
        {
            manufacture.Remove(id);
            return RedirectToAction("Index");
        }

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
