﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nexus_Service.BLL;
using Nexus_Service.DAL;

namespace Nexus_Service.Areas.Admin.Controllers
{
    [CustomizeAuthorize]
    public class CitiesController : Controller
    {
        private Connect_DB db = new Connect_DB();
        IRepository<City> _city;
        public CitiesController()
        {
            _city = new Repository<City>();
        }
        // GET: Admin/Cities
        public ActionResult Index()
        {
            return View(db.Cities.ToList());
        }

        // GET: Admin/Cities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        // GET: Admin/Cities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Cities/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CityId,Name")] City city)
        {
            if (db.Cities.Any(x => x.Name.Equals(city.Name)))
            {
                ModelState.AddModelError("Name", "This name is exists");
            }
            if (ModelState.IsValid)
            {
                db.Cities.Add(city);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(city);
        }

        // GET: Admin/Cities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            City city = db.Cities.Find(id);
            if (city == null)
            {
                return HttpNotFound();
            }
            return View(city);
        }

        // POST: Admin/Cities/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CityId,Name")] City city)
        {
            if (db.Cities.Any(x => x.Name.Equals(city.Name) && x.CityId != (city.CityId)))
            {
                ModelState.AddModelError("Name", "This name is exists");
            }
            if (ModelState.IsValid)
            {
                var citys = db.Cities.Find(city.CityId);
                citys.Name = city.Name;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(city);
        }

        // GET: Admin/Cities/Delete/5
        public ActionResult Delete(int id)
        {
            _city.Remove(id);
            return RedirectToAction("Index");
        }

      

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
