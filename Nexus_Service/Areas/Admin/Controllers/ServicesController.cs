﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nexus_Service.BLL;
using Nexus_Service.DAL;

namespace Nexus_Service.Areas.Admin.Controllers
{
    [CustomizeAuthorize]
    public class ServicesController : Controller
    {
        private Connect_DB db = new Connect_DB();
        IRepository<Service> _service;
        IRepository<Category> _category;
        public ServicesController()
        {
            _service = new Repository<Service>();
            _category = new Repository<Category>();
        }
        // GET: Admin/Services
        public ActionResult Index()
        {
            var services = db.Services.Include(s => s.category);
            return View(services.ToList());
        }

        // GET: Admin/Services/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = db.Services.Find(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            return View(service);
        }

        // GET: Admin/Services/Create
        public ActionResult Create()
        {
            ViewBag.CatId = new SelectList(_category.Get(), "CatId", "CatName").ToList();
            return View();
        }

        // POST: Admin/Services/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ServiceId,Name,Status,CatId")] Service service)
        {
            if (db.Services.Any(x => x.Name.Equals(service.Name)))
            {
                ModelState.AddModelError("Name", "This name is exists");
            }
            if (ModelState.IsValid)
            {
                db.Services.Add(service);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CatId = new SelectList(_category.Get(), "CatId", "CatName").ToList();
            return View(service);
        }

        // GET: Admin/Services/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Service service = db.Services.Find(id);
            if (service == null)
            {
                return HttpNotFound();
            }
            ViewBag.CatId = new SelectList(db.Categories, "CatId", "CatName", service.CatId);
            return View(service);
        }

        // POST: Admin/Services/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ServiceId,Name,Status,CatId")] Service service)
        {
            if (db.Services.Any(x => x.Name.Equals(service.Name)))
            {
                ModelState.AddModelError("Name", "This name is exists");
            }
            if (ModelState.IsValid)
            {
                db.Services.Add(service);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CatId = new SelectList(_category.Get(), "CatId", "CatName").ToList();
            return View(service);
        }

        // GET: Admin/Services/Delete/5
        public ActionResult Delete(int id)
        {
            _service.Remove(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
