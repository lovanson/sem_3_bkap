﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Nexus_Service.BLL;
using Nexus_Service.DAL;

namespace Nexus_Service.Areas.Admin.Controllers
{
    [CustomizeAuthorize]
    public class EmployeesController : Controller
    {
        IRepository<Account> _Account;
        IRepository<Employee> _Employee;
        public EmployeesController()
        {
            _Account = new Repository<Account>();
            _Employee = new Repository<Employee>();
        }
        private Connect_DB db = new Connect_DB();

        // GET: Admin/Employees
        public ActionResult Index()
        {
            var employees = db.Employees.Include(e => e.account).Include(e => e.groupUser);
            return View(employees.ToList());
        }

        // GET: Admin/Employees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Admin/Employees/Create
        public ActionResult Create()
        {
            ViewBag.AccountId = new SelectList(db.Accounts, "AccountId", "LoginId");
            ViewBag.EmpId = new SelectList(db.Emp_Order_Details, "EmpId", "EmpId");
            ViewBag.GroupId = new SelectList(db.GroupUsers, "Id", "GroupName");
            return View();
        }

        // POST: Admin/Employees/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EmpId,AccountId,GroupId,Status,Name,Phone,Email,DoB")] Employee employee ,Account account)
        {
            if (ModelState.IsValid)
            {
               var scccc = db.Accounts.Add(account);
                var idac = account.AccountId;
                employee.AccountId = idac;
                db.Employees.Add(employee);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AccountId = new SelectList(db.Accounts, "AccountId", "LoginId", employee.AccountId);
            ViewBag.EmpId = new SelectList(db.Emp_Order_Details, "EmpId", "EmpId", employee.EmpId);
            ViewBag.GroupId = new SelectList(db.GroupUsers, "Id", "GroupName", employee.GroupId);
            return View(employee);
        }

        // GET: Admin/Employees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            ViewBag.AccountId = new SelectList(db.Accounts, "AccountId", "LoginId", employee.AccountId);
            ViewBag.EmpId = new SelectList(db.Emp_Order_Details, "EmpId", "EmpId", employee.EmpId);
            ViewBag.GroupId = new SelectList(db.GroupUsers, "Id", "GroupName", employee.GroupId);
            return View(employee);
        }

        // POST: Admin/Employees/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EmpId,AccountId,GroupId,Status,Name,Phone,Email,DoB")] Employee employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AccountId = new SelectList(db.Accounts, "AccountId", "LoginId", employee.AccountId);
            ViewBag.EmpId = new SelectList(db.Emp_Order_Details, "EmpId", "EmpId", employee.EmpId);
            ViewBag.GroupId = new SelectList(db.GroupUsers, "Id", "GroupName", employee.GroupId);
            return View(employee);
        }

        // GET: Admin/Employees/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Admin/Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Employee employee = db.Employees.Find(id);
            db.Employees.Remove(employee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
